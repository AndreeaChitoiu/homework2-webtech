import React from 'react';
import axios from 'axios';

export class AddProduct extends React.Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.id=0;
        this.state.productName="";
        this.state.price=0;
    }
    
    handleChangeProductName= (event) =>{
        this.setState({
            productName: event.target.value
        })
    }
    
    handleChangePrice= (event) =>{
        this.setState({
            price: event.target.value
        })
    }
    
    handleAddClick = () => {
        let product = {
            productName: this.state.productName,
            price: this.state.price
        }
        
        let axiosConfig = {
             headers: {
                "Content-Type": "application/json",
                 "Access-Control-Allow-Origin": "*",
                 "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE"
                       }
                         };

        
        axios.post('https://homework2-webtech-andreeachitoiu.c9users.io:8081/add', product,axiosConfig).then((res) => {
            if(res.status === 200){
                this.props.productAdded(product)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
    
    render(){
        return(
            <div>
                <h1>Add Product</h1>
                <input type="text" placeholder="Product Name" 
                    onChange={this.handleChangeProductName}
                    value={this.state.productName} />
                <input type="number" value={this.state.price}
                    onChange={this.handleChangePrice} />
                <button onClick={this.handleAddClick}>Add Product</button>
            </div>
            );
    }
}