import React from 'react';

export class ProductList extends React.Component {
    constructor(props){
        super(props);
    }
    
    render(){
        let items = this.props.source.map((product, index) =>{
            return <div key={index}>{product.productName} - {product.price}</div>
        });
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div>
                {items}
                </div>
            </div>
        );
    }
}