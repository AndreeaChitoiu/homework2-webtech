import React, { Component } from 'react';

import './App.css';
import {ProductList} from './componente/productList/ProductList';
import {AddProduct} from './componente/addProduct/AddProduct';

class App extends Component {
  
  constructor(props){
    super(props);
    this.state={};
    this.state.products=[];
    this.state.doing=[];
    this.state.done=[];
  }
  
  onItemAdded=(product)=>{
   let newProducts=this.state.products;
   newProducts.push(product);
   this.setState({
     todos:newProducts
   });
  }
  
  
  componentDidMount(){
    fetch('https://homework2-webtech-andreeachitoiu.c9users.io:8081/get-all')
    .then((res)=>res.json())
    .then((newProducts)=>{
      this.setState({
        products:newProducts
      })
    })
    
  }
  render() {
    return (
      <React.Fragment>
      <AddProduct productAdded={this.onItemAdded}/>
      <ProductList title="Products" source={this.state.products}/>
      </React.Fragment>
    );
  }
}

export default App;
